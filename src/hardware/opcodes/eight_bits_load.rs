use super::super::cpu::{Opcode, CPU};

pub fn register_opcodes(cpu: &mut CPU) {
    cpu.register_opcode(
        0x0e,
        Box::new(Opcode::new(String::from("LD C,d8"), 2, 8, ld_c_d8))
    );
    cpu.register_opcode(
        0x1a,
        Box::new(Opcode::new(String::from("LD A,(DE)"), 1, 8, ld_a_de))
    );
    cpu.register_opcode(
        0x3e,
        Box::new(Opcode::new(String::from("LD A,d8"), 2, 8, ld_a_d8))
    );
    cpu.register_opcode(
        0x77,
        Box::new(Opcode::new(String::from("LD (HL),A"), 1, 8, ld_hl_a))
    );
    cpu.register_opcode(
        0xe0,
        Box::new(Opcode::new(String::from("LDH (a8),A"), 2, 12, ldh_a8_a))
    );
    cpu.register_opcode(
        0xe2,
        Box::new(Opcode::new(String::from("LD (C),A"), 1, 8, ld_c_a))
    );
}

fn ld_c_d8(cpu: &mut CPU) -> bool {
    let data = cpu.read_byte(cpu.get_program_counter() + 1);
    cpu.set_register_c(data);

    true
}

fn ld_a_de(cpu: &mut CPU) -> bool {
    cpu.set_register_a(cpu.read_byte(cpu.get_register_de()));

    true
}

fn ld_a_d8(cpu: &mut CPU) -> bool {
    let data = cpu.read_byte(cpu.get_program_counter() + 1);
    cpu.set_register_a(data);

    true
}

fn ld_hl_a(cpu: &mut CPU) -> bool {
    let addr = cpu.get_register_hl();

    cpu.write_byte(addr, cpu.get_register_a());

    true
}

fn ldh_a8_a(cpu: &mut CPU) -> bool {
    let addr = 0xFF00 + cpu.read_byte(cpu.get_program_counter() + 1) as u16;
    
    cpu.write_byte(addr, cpu.get_register_a());

    true
}

fn ld_c_a(cpu: &mut CPU) -> bool {
    let addr = 0xFF00 + cpu.get_register_c() as u16;

    cpu.write_byte(addr, cpu.get_register_a());

    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ld_c_d8() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x0E);
        cpu.write_byte(0xFF81, 0x12);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure register c is loaded and cpu moved forward
        assert_eq!(cpu.get_register_c(), 0x12);
        assert_eq!(cpu.get_program_counter(), 0xFF82);
    }

    #[test]
    fn test_ld_a_de() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.set_register_de(0xFFFE);
        cpu.write_byte(0xFF80, 0x1a);
        cpu.write_byte(0xFFFE, 0xaa);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure register a is loaded and cpu moved forward
        assert_eq!(cpu.get_register_a(), 0xaa);
        assert_eq!(cpu.get_program_counter(), 0xFF81);
    }

    #[test]
    fn test_ld_a_d8() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x3E);
        cpu.write_byte(0xFF81, 0x12);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure register a is loaded and cpu moved forward
        assert_eq!(cpu.get_register_a(), 0x12);
        assert_eq!(cpu.get_program_counter(), 0xFF82);
    }

    #[test]
    fn test_ld_hl_a() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.set_register_hl(0xFFFE);
        cpu.set_register_a(0x34);
        cpu.write_byte(0xFF80, 0x77);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure data was written and cpu moved forward
        assert_eq!(cpu.read_byte(0xFFFE), 0x34);
        assert_eq!(cpu.get_program_counter(), 0xFF81);
    }

    #[test]
    fn test_ldh_a8_a() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.set_register_a(0xDE);
        cpu.write_byte(0xFF80, 0xe0);
        cpu.write_byte(0xFF81, 0xFE);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure data was written and cpu moved forward
        assert_eq!(cpu.read_byte(0xFFFE), 0xDE);
        assert_eq!(cpu.get_program_counter(), 0xFF82);
    }

    #[test]
    fn test_ld_c_a() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.set_register_c(0xFA);
        cpu.set_register_a(0xFF);
        cpu.write_byte(0xFF80, 0xe2);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure data was written and cpu moved forward
        assert_eq!(cpu.read_byte(0xFFFA), 0xFF);
        assert_eq!(cpu.get_program_counter(), 0xFF81);
    }
}
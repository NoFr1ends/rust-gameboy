use std::fs::File;
use std::io::Read;

pub struct Memory {
    cartridge_rom_bank: u8,
    internal_ram_bank: u8,

    boot: [u8; 256],
    vram: [u8; 8192],
    ram: [u8; 8192],
    zero_page: [u8; 127],

    // todo: move hardware register to dedicated devices
    bg_palette_data: u8, 
    sound_channel_1: u8, // 0-5 sound length (w), 6-7 wave pattern duty (r/w)
    sound_channel_1_volume: u8, // 0-2 number of envelop sweep, 3 envelop direction, 4-7 initial volume of envelop sweep
    sound_on_off: u8, // 0 = Sound 1, 1 = Sound 2, 2 = Sound 3, 3 = Sound 4, 7 = All on/off
    sound_output_selection: u8,
    sound_channel_control: u8, // 0-2 SO1 volume, 3 SO1 enable, 4-6 SO2 volume, 7 SO2 enable
}

impl Memory {
    pub fn create_memory() -> Memory {
        // Load boot loader
        let boot_loader: [u8; 256] = match File::open("boot.gb") {
            Ok(mut file) => {
                let mut data: [u8; 256] = [0; 256];
                file.read(&mut data).unwrap();
                data
            },
            Err(why) => {
                println!("Failed to read bootloader {}", why);
                [0; 256]
            }
        };

        return Memory {
            cartridge_rom_bank: 0,
            internal_ram_bank: 0,
            boot: boot_loader,
            vram: [0; 8192],
            ram: [0; 8192],
            zero_page: [0; 127],
            bg_palette_data: 0,
            sound_on_off: 0,
            sound_channel_1: 0,
            sound_channel_1_volume: 0,
            sound_output_selection: 0,
            sound_channel_control: 0,
        }
    }

    /**
     * $0000-$00FF Restart and Interrupt Vectors
     * $0100-$014F Cartridge Header Area
     * $0150-$3FFF Cartridge ROM - Bank 0 (fixed)
     * $4000-$7FFF Cartridge ROM - Switchable Banks 1-xx
     * $8000-$97FF Character RAM
     * $9800-$9BFF BG Map Data 1
     * $9C00-$9FFF BG Map Data 2
     * $A000-$BFFF Cartridge RAM (If Available)
     * $C000-$CFFF Internal RAM - Bank 0 (fixed)
     * $D000-$DFFF Internal RAM - Bank 1-7 (switchable - CGB only)
     * $E000-$FDFF Echo RAM - Reserved, Do Not Use
     * $FE00-$FE9F OAM - Object Attribute Memory
     * $FEA0-$FEFF Unusable Memory
     * $FF00-$FF7F Hardware I/O Registers
     * $FF80-$FFFE Zero Page - 127 bytes
     * $FFFF       Interrupt Enable Flag
     */
    pub fn read_byte(&self, addr: u16) -> u8 {
        if addr <= 0x00FF {
            return self.boot[addr as usize]
        }
        if addr >= 0xFF80 && addr <= 0xFFFE {
            return self.zero_page[addr as usize - 0xFF80];
        }
        if addr == 0xFF11 {
            return self.sound_channel_1 & 0b1100_0000;
        }
        if addr == 0xFF12 {
            return self.sound_channel_1_volume;
        }
        if addr == 0xFF24 {
            return self.sound_channel_control;
        }
        if addr == 0xFF25 {
            return self.sound_output_selection;
        }
        if addr == 0xFF47 {
            return self.bg_palette_data;
        }
        if addr >= 0xC000 && addr <= 0xDFFF {
            return self.ram[addr as usize - 0xC000];
        }
        if addr >= 0x8000 && addr <= 0x9FFF {
            return self.vram[addr as usize - 0x8000];
        }

        panic!("Read from unknown memory location 0x{:x}", addr);
        //println!("Read from unknown memory location {}", addr);
        //return 0;
    }

    pub fn write_byte(&mut self, addr: u16, data: u8) {
        if addr >= 0x0150 && addr <= 0x7FFF {
            self.cartridge_rom_bank = data;
            return;
        }
        if addr >= 0xFF80 && addr <= 0xFFFE {
            self.zero_page[addr as usize - 0xFF80] = data;
            return;
        }
        if addr == 0xFF11 {
            self.sound_channel_1 = data;
            return;
        }
        if addr == 0xFF12 {
            self.sound_channel_1_volume = data;
            return;
        }
        if addr == 0xFF24 {
            self.sound_channel_control = data;
            return;
        }
        if addr == 0xFF25 {
            self.sound_output_selection = data;
            return;
        }
        if addr == 0xFF26 {
            self.sound_on_off = data;
            return;
        }
        if addr == 0xFF47 {
            self.bg_palette_data = data;
            return;
        }
        if addr >= 0xC000 && addr <= 0xDFFF {
            self.ram[addr as usize - 0xC000] = data;
            return;
        }
        if addr >= 0x8000 && addr <= 0x9FFF {
            self.vram[addr as usize - 0x8000] = data;
            return;
        }

        panic!("Write to unknown memory location 0x{:x}", addr);
        //println!("Write to unknown memory location {}", addr);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cartridge_bank_switching() {
        let mut memory = Memory::create_memory();

        memory.write_byte(0x2222, 2);
        assert_eq!(memory.cartridge_rom_bank, 2);

        memory.write_byte(0x6DDD, 1);
        assert_eq!(memory.cartridge_rom_bank, 1);

        memory.write_byte(0x7FFF, 4);
        assert_eq!(memory.cartridge_rom_bank, 4);

        // Check if cartridge rom bank stays the same when writing to another location
        memory.write_byte(0xFF80, 6);
        assert_eq!(memory.cartridge_rom_bank, 4);
    }

    #[test]
    fn test_zero_page() {
        let mut memory = Memory::create_memory();

        let mut a: u8 = 0;
        for i in 0xFF80..0xFFFF {
            memory.write_byte(i, a);
            a+=1;
        }

        a = 0;

        // Verify read byte
        for i in 0xFF80..0xFFFF {
            assert_eq!(memory.read_byte(i), a);
            a+=1;
        }

        // Verify it ended inside zero page array
        for i in 0..127 {
            assert_eq!(memory.zero_page[i], i as u8)
        }
    }

    #[test]
    fn test_ram() {
        let mut memory = Memory::create_memory();

        let mut a: u8 = 0;
        for i in 0xC000..0xE000 {
            memory.write_byte(i, a);
            a = if a >= 255 {
                0
            } else {
                a + 1
            }
        }

        a = 0;

        // Verify read
        for i in 0xC000..0xE000 {
            assert_eq!(memory.read_byte(i), a);
            a = if a >= 255 {
                0
            } else {
                a + 1
            }
        }

        // Make sure it got into correct ram array
        for i in 0..8_192 {
            assert_eq!(memory.ram[i], (i & 0xFF) as u8);
        }
    }
}
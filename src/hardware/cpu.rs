use super::memory::{Memory};
use super::opcodes::sixteen_bits_memory::{register_opcodes as sm_register_opcodes};
use super::opcodes::eight_bits_arithmetic::{register_opcodes as eb_register_opcodes};
use super::opcodes::eight_bits_load::{register_opcodes as el_register_opcodes};
use super::opcodes::cb::{register_opcodes as cb_register_opcodes};
use super::opcodes::jump::{register_opcodes as jp_register_opcodes};
use std::collections::HashMap;

pub struct Opcode {
    instruction_size: u8, // size with opcode
    cycles: i8, // clock cylces this operation needs
    display: String, // used for debugging
    callback: fn(&mut CPU) -> bool, // opcode callback logic, if this method returns true the stack pointer will be adjusted
}

impl Opcode {
    pub fn new(display: String, instruction_size: u8, cycles: i8, callback: fn(&mut CPU) -> bool) -> Opcode {
        Opcode {
            instruction_size: instruction_size,
            cycles: cycles,
            display: display,
            callback: callback
        }
    }
}

pub struct CPU {
    sp: u16, // Stack Pointer
    pc: u16, // Program Counter

    af: u16, // A & F register
    bc: u16, // B & C register
    de: u16, // D & E register
    hl: u16, // H & L register

    memory: Memory, // Memory Bus (also includes gpu memory etc)

    in_cb: bool,
    opcodes: HashMap<u8, Box<Opcode>>, // Dictionary of all known and valid opcodes
    cb_opcodes: HashMap<u8, Box<Opcode>>, // Dictionary of all known and valid opcodes
}

impl CPU {
    pub fn create_cpu() -> CPU {
        let mut cpu = CPU {
            sp: 0,
            pc: 0,
            af: 0,
            bc: 0,
            de: 0,
            hl: 0,
            memory: Memory::create_memory(),
            in_cb: false,
            opcodes: HashMap::new(),
            cb_opcodes: HashMap::new(),
        };

        sm_register_opcodes(&mut cpu);
        eb_register_opcodes(&mut cpu);
        el_register_opcodes(&mut cpu);
        cb_register_opcodes(&mut cpu);
        jp_register_opcodes(&mut cpu);

        cpu
    }

    pub fn register_opcode(&mut self, opcode: u8, definition: Box<Opcode>) {
        self.opcodes.insert(opcode, definition);
    }

    pub fn register_cb_opcode(&mut self, opcode: u8, definition: Box<Opcode>) {
        self.cb_opcodes.insert(opcode, definition);
    }

    fn get_opcode(&self, opcode_id: u8) -> Option<&Box<Opcode>> {
        if self.opcodes.contains_key(&opcode_id) {
            return self.opcodes.get(&opcode_id);
        }

        return None;
    }

    fn get_cb_opcode(&self, opcode_id: u8) -> Option<&Box<Opcode>> {
        if self.cb_opcodes.contains_key(&opcode_id) {
            return self.cb_opcodes.get(&opcode_id);
        }

        return None;
    }

    pub fn step(&mut self) {
        // Load current opcode
        let pc = self.pc;
        let opcode_id = self.read_byte(pc);

        println!("Step at 0x{:x}", pc);

        if opcode_id == 0xCB {
            println!("CB prefix");
            self.in_cb = true;
            self.pc += 1;
            self.step();
            return;
        }

        // Lookup opcode in dictionary
        let opcode = if !self.in_cb {
            match self.get_opcode(opcode_id) {
                Some(opcode) => opcode.clone(),
                None => panic!("Unknown opcode 0x{:x}", opcode_id)
            }
        } else {
            match self.get_cb_opcode(opcode_id) {
                Some(opcode) => opcode.clone(),
                None => panic!("Unknown cb opcode 0x{:x}", opcode_id)
            }
        };

        let size = opcode.instruction_size as u16;

        println!("Executing {}", opcode.display);
        if (opcode.callback)(self) {
            self.set_program_counter(pc + size);
        }

        self.in_cb = false;
    }

    pub fn read_byte(&self, addr: u16) -> u8 {
        self.memory.read_byte(addr)
    }

    pub fn write_byte(&mut self, addr: u16, data: u8) {
        self.memory.write_byte(addr, data);
    }

    pub fn get_stack_pointer(&self) -> u16 {
        self.sp
    }

    pub fn set_stack_pointer(&mut self, value: u16) {
        self.sp = value;
    }

    pub fn get_program_counter(&self) -> u16 {
        self.pc
    }

    pub fn set_program_counter(&mut self, value: u16) {
        self.pc = value;
    }

    pub fn get_register_a(&self) -> u8 {
        ((self.af >> 8) & 0xFF) as u8
    }

    pub fn get_register_f(&self) -> u8 {
        (self.af & 0xFF) as u8
    }

    pub fn set_register_a(&mut self, value: u8) {
        self.af = (value as u16) << 8 | self.get_register_f() as u16;
    }

    pub fn set_register_f(&mut self, value: u8) {
        self.af = (self.get_register_a() as u16) << 8 | value as u16;
    }

    pub fn set_register_af(&mut self, value: u16) {
        self.af = value;
    }
    
    pub fn get_register_af(&self) -> u16 {
        self.af
    }

    pub fn get_register_b(&self) -> u8 {
        ((self.bc >> 8) & 0xFF) as u8
    }

    pub fn get_register_c(&self) -> u8 {
        (self.bc & 0xFF) as u8
    }

    pub fn set_register_b(&mut self, value: u8) {
        self.bc = (value as u16) << 8 | self.get_register_c() as u16;
    }

    pub fn set_register_c(&mut self, value: u8) {
        self.bc = (self.get_register_b() as u16) << 8 | value as u16;
    }

    pub fn set_register_bc(&mut self, value: u16) {
        self.bc = value;
    }

    pub fn get_register_bc(&self) -> u16 {
        self.bc
    }

    pub fn get_register_d(&self) -> u8 {
        ((self.de >> 8) & 0xFF) as u8
    }

    pub fn get_register_e(&self) -> u8 {
        (self.de & 0xFF) as u8
    }

    pub fn set_register_d(&mut self, value: u8) {
        self.de = (value as u16) << 8 | self.get_register_e() as u16;
    }

    pub fn set_register_e(&mut self, value: u8) {
        self.de = (self.get_register_d() as u16) << 8 | value as u16;
    }

    pub fn set_register_de(&mut self, value: u16) {
        self.de = value;
    }

    pub fn get_register_de(&self) -> u16 {
        self.de
    }

    pub fn get_register_h(&self) -> u8 {
        ((self.hl >> 8) & 0xFF) as u8
    }

    pub fn get_register_l(&self) -> u8 {
        (self.hl & 0xFF) as u8
    }

    pub fn set_register_h(&mut self, value: u8) {
        self.hl = (value as u16) << 8 | self.get_register_l() as u16;
    }

    pub fn set_register_l(&mut self, value: u8) {
        self.hl = (self.get_register_h() as u16) << 8 | value as u16;
    }

    pub fn set_register_hl(&mut self, value: u16) {
        self.hl = value;
    }

    pub fn get_register_hl(&self) -> u16 {
        self.hl
    }

    // Flags
    pub fn set_zero_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags |= 0b1000_0000;
        self.set_register_f(flags);
    }
    pub fn unset_zero_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags &= 0b0111_1111;
        self.set_register_f(flags);
    }
    pub fn has_zero_flag(&self) -> bool {
        (self.get_register_f() & 0b1000_0000) == 0b1000_0000
    }

    pub fn set_subtract_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags |= 0b0100_0000;
        self.set_register_f(flags);
    }
    pub fn unset_subtract_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags &= 0b1011_1111;
        self.set_register_f(flags);
    }
    pub fn has_subtract_flag(&self) -> bool {
        (self.get_register_f() & 0b0100_0000) == 0b0100_0000
    }

    pub fn set_half_carry_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags |= 0b0010_0000;
        self.set_register_f(flags);
    }
    pub fn unset_half_carry_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags &= 0b1101_1111;
        self.set_register_f(flags);
    }
    pub fn has_half_carry_flag(&self) -> bool {
        (self.get_register_f() & 0b0010_0000) == 0b0010_0000
    }

    pub fn set_carry_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags |= 0b0001_0000;
        self.set_register_f(flags);
    }
    pub fn unset_carry_flag(&mut self) {
        let mut flags = self.get_register_f();
        flags &= 0b1110_1111;
        self.set_register_f(flags);
    }
    pub fn has_carry_flag(&self) -> bool {
        (self.get_register_f() & 0b0001_0000) == 0b0001_0000
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_register_af() {
        let mut cpu = CPU::create_cpu();

        cpu.set_register_af(0xADEF);
        assert_eq!(cpu.get_register_a(), 0xAD);
        assert_eq!(cpu.get_register_f(), 0xEF);

        cpu.set_register_a(0xFF);
        assert_eq!(cpu.get_register_a(), 0xFF);
        assert_eq!(cpu.get_register_f(), 0xEF);

        cpu.set_register_f(0x00);
        assert_eq!(cpu.get_register_a(), 0xFF);
        assert_eq!(cpu.get_register_f(), 0x00);

        assert_eq!(cpu.get_register_af(), 0xFF00);
    }   

    #[test]
    fn test_register_bc() {
        let mut cpu = CPU::create_cpu();

        cpu.set_register_bc(0xADEF);
        assert_eq!(cpu.get_register_b(), 0xAD);
        assert_eq!(cpu.get_register_c(), 0xEF);

        cpu.set_register_b(0xFF);
        assert_eq!(cpu.get_register_b(), 0xFF);
        assert_eq!(cpu.get_register_c(), 0xEF);

        cpu.set_register_c(0x00);
        assert_eq!(cpu.get_register_b(), 0xFF);
        assert_eq!(cpu.get_register_c(), 0x00);

        assert_eq!(cpu.get_register_bc(), 0xFF00);
    } 

    #[test]
    fn test_register_de() {
        let mut cpu = CPU::create_cpu();

        cpu.set_register_de(0xADEF);
        assert_eq!(cpu.get_register_d(), 0xAD);
        assert_eq!(cpu.get_register_e(), 0xEF);

        cpu.set_register_d(0xFF);
        assert_eq!(cpu.get_register_d(), 0xFF);
        assert_eq!(cpu.get_register_e(), 0xEF);

        cpu.set_register_e(0x00);
        assert_eq!(cpu.get_register_d(), 0xFF);
        assert_eq!(cpu.get_register_e(), 0x00);

        assert_eq!(cpu.get_register_de(), 0xFF00);
    } 

    #[test]
    fn test_register_hl() {
        let mut cpu = CPU::create_cpu();

        cpu.set_register_hl(0xADEF);
        assert_eq!(cpu.get_register_h(), 0xAD);
        assert_eq!(cpu.get_register_l(), 0xEF);

        cpu.set_register_h(0xFF);
        assert_eq!(cpu.get_register_h(), 0xFF);
        assert_eq!(cpu.get_register_l(), 0xEF);

        cpu.set_register_l(0x00);
        assert_eq!(cpu.get_register_h(), 0xFF);
        assert_eq!(cpu.get_register_l(), 0x00);

        assert_eq!(cpu.get_register_hl(), 0xFF00);
    } 

    #[test]
    fn test_stack_pointer() {
        let mut cpu = CPU::create_cpu();

        cpu.set_stack_pointer(0x4531);
        assert_eq!(cpu.get_stack_pointer(), 0x4531);
    }

    #[test]
    fn test_program_counter() {
        let mut cpu = CPU::create_cpu();

        cpu.set_program_counter(0x4531);
        assert_eq!(cpu.get_program_counter(), 0x4531);
    }
}
use super::super::cpu::{Opcode, CPU};

pub fn register_opcodes(cpu: &mut CPU) {
    cpu.register_opcode(
        0x0c, 
        Box::new(Opcode::new(String::from("INC C"), 1, 4, inc_c))
    );
    cpu.register_opcode(
        0xaf, 
        Box::new(Opcode::new(String::from("XOR A"), 1, 4, xor_a))
    );
}

fn inc_c(cpu: &mut CPU) -> bool {
    let c = cpu.get_register_c();

    if c == 255 {
        cpu.set_zero_flag();
        cpu.set_half_carry_flag();
        cpu.set_register_c(0);
    } else {
        cpu.unset_zero_flag();
        cpu.unset_half_carry_flag();
        cpu.set_register_c(c + 1);
    }

    true
}

fn xor_a(cpu: &mut CPU) -> bool {
    let a = cpu.get_register_a();
    let a = (a ^ a) & 0xFF;
    if a == 0 {
        // set zero flag
        cpu.set_register_f(0x80);
    } else {
        // clear flags
        cpu.set_register_f(0x00);
    }
    cpu.set_register_a(a);
    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_inc_c() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_c(0x00);
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x0c);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make register c incremented and flags are right
        assert_eq!(cpu.get_register_c(), 0x01);
        assert_eq!(cpu.has_zero_flag(), false);
        assert_eq!(cpu.has_half_carry_flag(), false);
    }

    #[test]
    fn test_inc_c_overflow() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_c(0xFF);
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x0c);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make register c incremented and flags are right
        assert_eq!(cpu.get_register_c(), 0x00);
        assert_eq!(cpu.has_zero_flag(), true);
        assert_eq!(cpu.has_half_carry_flag(), true);
    }

    #[test]
    fn test_xor_a() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_a(0xFF);
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0xaf);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure register a is correct and the cpu moved forward
        assert_eq!(cpu.get_register_a(), 0x00);
        assert_eq!(cpu.get_register_f(), 0x80); // maker sure zero flag is set
        assert_eq!(cpu.get_program_counter(), 0xFF81);
    }
}
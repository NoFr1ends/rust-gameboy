use super::super::cpu::{Opcode, CPU};

pub fn register_opcodes(cpu: &mut CPU) {
    cpu.register_opcode(
        0x11, 
        Box::new(Opcode::new(String::from("LD DE,d16"), 3, 12, ld_de_d16))
    );
    cpu.register_opcode(
        0x21, 
        Box::new(Opcode::new(String::from("LD HL,d16"), 3, 12, ld_hl_d16))
    );
    cpu.register_opcode(
        0x31, 
        Box::new(Opcode::new(String::from("LD SP,d16"), 3, 12, ld_sp_d16))
    );
    cpu.register_opcode(
        0x32, 
        Box::new(Opcode::new(String::from("LD (HL-),A"), 1, 8, ld_hld_a))
    );
}

fn ld_de_d16(cpu: &mut CPU) -> bool {
    let lower_byte = cpu.read_byte(cpu.get_program_counter() + 1);
    let higher_byte = cpu.read_byte(cpu.get_program_counter() + 2);

    let de = lower_byte as u16 | ((higher_byte as u16) << 8);

    cpu.set_register_de(de);
    true
}

fn ld_hl_d16(cpu: &mut CPU) -> bool {
    let lower_byte = cpu.read_byte(cpu.get_program_counter() + 1);
    let higher_byte = cpu.read_byte(cpu.get_program_counter() + 2);

    let hl = lower_byte as u16 | ((higher_byte as u16) << 8);

    cpu.set_register_hl(hl);
    true
}

fn ld_sp_d16(cpu: &mut CPU) -> bool {
    let lower_byte = cpu.read_byte(cpu.get_program_counter() + 1);
    let higher_byte = cpu.read_byte(cpu.get_program_counter() + 2);

    let sp = lower_byte as u16 | ((higher_byte as u16) << 8);

    cpu.set_stack_pointer(sp);
    true
}

// Write register A into address HL and decrement HL
fn ld_hld_a(cpu: &mut CPU) -> bool {
    let hl = cpu.get_register_hl();
    
    cpu.write_byte(hl, cpu.get_register_a());
    cpu.set_register_hl(hl - 1);

    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ld_de_d16() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x11);
        cpu.write_byte(0xFF81, 0xAB);
        cpu.write_byte(0xFF82, 0xCD);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure register h & l are correct and the cpu moved forward
        assert_eq!(cpu.get_register_de(), 0xCDAB);
        assert_eq!(cpu.get_program_counter(), 0xFF83);
    }

    #[test]
    fn test_ld_hl_d16() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x21);
        cpu.write_byte(0xFF81, 0xAB);
        cpu.write_byte(0xFF82, 0xCD);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure register h & l are correct and the cpu moved forward
        assert_eq!(cpu.get_register_hl(), 0xCDAB);
        assert_eq!(cpu.get_program_counter(), 0xFF83);
    }

    #[test]
    fn test_ld_sp_d16() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.write_byte(0xFF80, 0x31);
        cpu.write_byte(0xFF81, 0xFE);
        cpu.write_byte(0xFF82, 0xFF);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure the stack pointer is correct and the cpu moved forward
        assert_eq!(cpu.get_stack_pointer(), 0xFFFE);
        assert_eq!(cpu.get_program_counter(), 0xFF83);
    }

    #[test]
    fn test_ld_hld_a() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_program_counter(0xFF80);
        cpu.set_register_hl(0xFFFE);
        cpu.set_register_a(0x11);
        cpu.write_byte(0xFF80, 0x32);
        cpu.write_byte(0xFFFE, 0xDE);

        // Let the cpu execute the next instruction
        cpu.step();

        // Make sure address was written
        assert_eq!(cpu.read_byte(0xFFFE), 0x11);
        assert_eq!(cpu.get_program_counter(), 0xFF81);
    }
}
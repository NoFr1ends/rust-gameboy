pub use self::cpu::CPU;
pub use self::memory::Memory;

mod cpu;
mod memory;
mod opcodes {
    pub mod sixteen_bits_memory;
    pub mod eight_bits_arithmetic;
    pub mod eight_bits_load;
    pub mod cb;
    pub mod jump;
}
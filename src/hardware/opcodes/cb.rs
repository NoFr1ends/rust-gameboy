use super::super::cpu::{Opcode, CPU};

pub fn register_opcodes(cpu: &mut CPU) {
    cpu.register_cb_opcode(
        0x7c, 
        Box::new(Opcode::new(String::from("BIT 7, H"), 1, 8, bit_7_h))
    );
}

fn bit_7_h(cpu: &mut CPU) -> bool {
    let h = cpu.get_register_h();
    let bit = h & 0b1000_0000;
    if bit == 0b1000_0000 {
        cpu.unset_zero_flag();
    } else {
        cpu.set_zero_flag();
    }

    cpu.unset_subtract_flag();
    cpu.set_half_carry_flag();
    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bit_7_h_positive() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_h(0b1000_0000);
        cpu.set_program_counter(0xFF80);
        cpu.set_carry_flag();
        cpu.write_byte(0xFF80, 0xcb);
        cpu.write_byte(0xFF81, 0x7c);

        // Execute instruction
        cpu.step();

        // Validate flags
        assert_eq!(cpu.has_zero_flag(), false);
        assert_eq!(cpu.has_subtract_flag(), false);
        assert_eq!(cpu.has_half_carry_flag(), true);
        // Make sure carry flag is unchanged
        assert_eq!(cpu.has_carry_flag(), true);
    }  

    #[test]
    fn test_bit_7_h_negative() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_h(0b0111_1111);
        cpu.set_program_counter(0xFF80);
        cpu.set_carry_flag();
        cpu.write_byte(0xFF80, 0xcb);
        cpu.write_byte(0xFF81, 0x7c);

        // Execute instruction
        cpu.step();

        // Validate flags
        assert_eq!(cpu.has_zero_flag(), true);
        assert_eq!(cpu.has_subtract_flag(), false);
        assert_eq!(cpu.has_half_carry_flag(), true);
        // Make sure carry flag is unchanged
        assert_eq!(cpu.has_carry_flag(), true);
    }  
}
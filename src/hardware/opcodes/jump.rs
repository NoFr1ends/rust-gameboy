use super::super::cpu::{Opcode, CPU};

pub fn register_opcodes(cpu: &mut CPU) {
    cpu.register_opcode(
        0x20,
        Box::new(Opcode::new(String::from("JR NZ,r8"), 2, 8, jr_nz_r8))
    );
}

fn jr_nz_r8(cpu: &mut CPU) -> bool {
    if !cpu.has_zero_flag() {
        let relative = cpu.read_byte(cpu.get_program_counter() + 1) as i8;
        if relative < 0 {
            cpu.set_program_counter(cpu.get_program_counter().wrapping_add(relative as u16 + 2));
        } else {
            cpu.set_program_counter(cpu.get_program_counter().wrapping_add(relative as u16));
        }
        false
    } else {
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_jr_nz_r8_backward() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_h(0b1000_0000);
        cpu.set_program_counter(0xFF80);
        cpu.unset_zero_flag();
        cpu.write_byte(0xFF80, 0x20);
        cpu.write_byte(0xFF81, 0xFB); // -5 bytes -> 0xFF7D
        
        // Execute instruction
        cpu.step();

        assert_eq!(cpu.get_program_counter(), 0xFF7D);
    }

    #[test]
    fn test_jr_nz_r8_forward() {
        // Setup test instruction
        let mut cpu = CPU::create_cpu();
        cpu.set_register_h(0b1000_0000);
        cpu.set_program_counter(0xFF80);
        cpu.unset_zero_flag();
        cpu.write_byte(0xFF80, 0x20);
        cpu.write_byte(0xFF81, 0x02); // 2 bytes -> 0xFF82
        
        // Execute instruction
        cpu.step();

        assert_eq!(cpu.get_program_counter(), 0xFF82);
    }
}